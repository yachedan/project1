<?php

class Group{
    private $id;
    private $number;
    private $amount;

    function __construct()
    {
        $arguments = func_get_args();
        $numberOfArguments = func_num_args();

        if(method_exists($this, $function = '__construct'.$numberOfArguments))
            call_user_func_array(array($this, $function), $arguments);
    }
    function __construct1()
    {
        $this->id = null;
        $this->number = null;
        $this->amount = null;
    }
    function __construct2( $number,  $name)
    {
        $this->id = null;
        $this->number = $number;
        $this->amount = $name;
    }
    function __construct3($id, $number,  $name)
    {
        $this->id = $id;
        $this->number = $number;
        $this->amount = $name;
    }
    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number): void
    {
        $this->number = $number;
    }
    public function getAmount()
    {
        return $this->amount;
    }
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    public function __toString()
    {
        return "Group=( id: $this->id, number: $this->number, amount: $this->amount)";

    }
}
class Student{
    private $id;
    private $number;
    private $name;
    private $group;

    function __construct()
    {
        $arguments = func_get_args();
        $numberOfArguments = func_num_args();

        if(method_exists($this, $function = '__construct'.$numberOfArguments))
            call_user_func_array(array($this, $function), $arguments);
    }
    function __construct1()
    {
        $this->id = null;
        $this->number = null;
        $this->name = null;

    }
    function __construct3( $number,  $name, $group)
    {
        $this->id = null;
        $this->number = $number;
        $this->name = $name;
        $group->setAmount($group->getAmount()+1);
        $this->group = $group;
    }
    /*function __construct3($id, $number,  $name)
    {
        $this->id = $id;
        $this->number = $number;
        $this->name = $name;
    }*/
    function __construct4($id, $number,  $name, $group)
    {
        $this->id = $id;
        $this->number = $number;
        $this->name = $name;
        $group->setAmount($group->getAmount()+1);
        $this->group = $group;

    }


    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }
    public function getNumber()
    {
        return $this->number;
    }
    public function setNumber($number)
    {
        $this->number = $number;
    }
    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getGroup()
    {
        return $this->group;
    }
    public function setGroup($group): void
    {
        $this->group = $group;
    }
    

    public function __toString()
    {
        return "Student=( id: $this->id, number: $this->number, name: $this->name, $this->group)";

    }
}
class TuitionStudent extends Student{
    private $paid;

    public function __construct5($number, $name,$group, $paid){
        parent::__construct3($number,$name,$group);
        $this->paid = $paid;
    }
    public function __construct6($id, $number,$name,$group,$paid){
        parent::__construct4($id,$number,$name,$group);
        $this->paid = $paid;
    }
    public function getPaid()
    {
        return $this->paid;
    }
    public function setPaid($paid){
        $this->paid=$paid;
    }
    public function __toString(){

        return "TuitionStudent=( id: {$this->getId()} , number: {$this->getNumber()}, name: {$this->getName()},
         paid: $this->paid, {$this->getGroup()})";
    }
}
$group = new Group("1", "125", 0);
echo $student = new Student("1", "PK12345678", "Don Quixote",$group);
echo '<br>';
echo $paidstudent = new TuitionStudent("PK1000000","Sancho Panza",true,$group);
